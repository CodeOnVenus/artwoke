<?php

namespace App\Service;

class UploaderHelper
{
    const URL = 'https://artwoke.s3.eu-west-3.amazonaws.com/';

    public function getPublicPath(string $path): string
    {
        return self::URL . $path;
    }
}
