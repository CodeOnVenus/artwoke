<?php

namespace App\Controller;

use App\Entity\Boards;
use App\Entity\Comments;
use App\Form\BoardsType;
use App\Form\CommentsFormType;
use App\Repository\UserRepository;
use App\Repository\BoardsRepository;
use App\Repository\CommentsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class BoardsController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    #[Route('/', name: 'app_boards', methods: ['GET'])]
    public function index(BoardsRepository $boardsRepository): Response
    {
        if($this->getUser() === null){
            $this->addFlash("info", "Register or Log in to share and woke your art !");
        }
        $boards = $boardsRepository->findBy([], ['createdAt' => 'DESC']);
        return $this->render('boards/index.html.twig', [
            'boards' => $boardsRepository->findAll(),
        ]);
    }

    #[IsGranted("ROLE_USER")]
    #[Route('/new', name: 'app_boards_new', methods: ['GET', 'POST'])]
    public function new(Request $request, BoardsRepository $boardsRepository, UserRepository $userRepo): Response
    {
        $val = 'create';
        $board = new Boards();
        $board->setUser($this->getUser());
        $form = $this->createForm(BoardsType::class, $board);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', "Board has been successfully created");
            $boardsRepository->add($board, true);
            
            return $this->redirectToRoute('app_boards', [], Response::HTTP_SEE_OTHER);
        }
        

        return $this->renderForm('boards/new.html.twig', [
            'board' => $board,
            'form' => $form,
            'val' => $val
        ]);
    }

    #[IsGranted("ROLE_USER")]
    #[Route('/{id<[0-9]+>}', name: 'app_boards_show', methods: ['GET', 'POST'])]
    public function show(Boards $board, CommentsRepository $commentRepo, Request $request): Response
    {
        $comments = $commentRepo->findAll();
        $comment = new Comments();
        $commentForm = $this->createForm(CommentsFormType::class);

        $commentForm->handleRequest($request);
        $description = $commentForm['description'] ->getData();

        if ($commentForm->isSubmitted() && $commentForm->isValid()) {
            $comment->setDescription($description);
            $comment->setUser($this->getUser());
            $comment->setBoard($board);

            $this->em->persist($comment);
            $this->em->flush();

            $this->addFlash('success', 'Votre commentaire a bien été posté');
            return $this->redirectToRoute('app_boards_show', ['id' => $board->getId()]);
        }
        return $this->render('boards/show.html.twig', [
            'board' => $board,
            'commentForm' => $commentForm->createView(),
            'comments' => $comments
        ]);
    }

    #[IsGranted("ROLE_USER")]
    #[Route('/{id<[0-9]+>}/edit', name: 'app_boards_edit', methods: ['GET', 'POST'])]
    #[IsGranted("BOARD_MANAGE", subject: "board")]
    public function edit(Request $request, Boards $board, BoardsRepository $boardsRepository): Response
    {
        $form = $this->createForm(BoardsType::class, $board);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->addFlash('success', "Board has been successfully edited");
            $boardsRepository->add($board, true);

            return $this->redirectToRoute('app_boards', [
                '$board' => $board
            ], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('boards/edit.html.twig', [
            'board' => $board,
            'form' => $form,
        ]);
    }

    #[IsGranted("ROLE_USER")]
    #[Route('/{id<[0-9]+>}/delete', name: 'app_boards_delete', methods: ['POST'])]
    public function delete(Request $request, Boards $board, BoardsRepository $boardsRepository): Response
    {
        $token = $request->request->get('_token');
        if ($this->isCsrfTokenValid('delete' . $board->getId(), $token)) {
            $this->addFlash('info', "Votre tableau a été supprimé avec succès");
            $boardsRepository->remove($board, true);
            $this->em->remove($board);
            $this->em->flush();
            return $this->redirectToRoute('app_boards');
        }
        throw new InvalidCsrfTokenException("Le token n'est pas valide");
    }
}
