<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Form\ChangePasswordFormType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AccountController extends AbstractController
{
    #[Route('/account/{name}', name: 'app_account_show', methods:['GET'])]
    public function show(User $user): Response
    {
        $boards = $user->getBoards()->getValues();
    
        return $this->render('account/show.html.twig', [
            'user' => $user,
            'boards' => $boards
        ]);
    }

    #[Route('/account/{name}/edit', name: 'app_account_edit', methods: 'GET|POST')]
    public function edit(Request $request, EntityManagerInterface $em): Response
    {
        $user = $this->getUser();
        $name = $user->getName();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('success', 'Vos informations ont bien été modifiées');
            return $this->redirectToRoute('app_account_show', [
                'name' => $name
            ]);
        }

        return $this->render('account/edit.html.twig', [
            "form" => $form->createView(),
            "user" => $user
        ]);
    }

    #[Route('/account/{name}/change-password', name: 'app_account_change_password', methods: 'GET|POST')]
    public function changePassword(Request $request, EntityManagerInterface $em, UserPasswordHasherInterface $passwordEncoder): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(ChangePasswordFormType::class, [
            'current_password_is_required' => true
        ]);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($passwordEncoder->hashPassword($user, $form['plainPassword']->getData()));
            $em->flush();
            $this->addFlash('success', 'Votre mot de passe a été modifiée avec succès');
            return $this->redirectToRoute('app_account_show', ['name' => $user->getName()]);
        }
        return $this->render('account/change-password.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
