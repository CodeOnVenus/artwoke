<?php

namespace App\DataFixtures;

use DateTime;
use App\Entity\User;
use App\Entity\Boards;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class DataFixtures extends Fixture
{
    private $hash;

    public function __construct(UserPasswordHasherInterface $hash)
    {
        $this->hash = $hash;
    }
    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $password = $this->hash->hashPassword($user, 'password');
        $user->setName('John Doe')
            ->setPassword($password)
            ->setEmail('john.doe@mail.com')
            ->setDescription('description')
            ->setImage('image.jpg');

        $board = new Boards();
        $board->setTitle('title')
                ->setDescription('description')
                ->setCreatedAt(new DateTime())
                ->setImage('image')
                ->setUser($user)
                ;
        $manager->persist($board);

        $manager->flush();
    }
}
