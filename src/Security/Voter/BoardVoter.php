<?php

namespace App\Security\Voter;

use App\Entity\Boards;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;

class BoardVoter extends Voter
{
    protected function supports($attribute, $subject) : bool
    {
        return $attribute === 'BOARD_CREATE' || (in_array($attribute, ['BOARD_MANAGE'])
            && $subject instanceof Boards);
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token) : bool
    {
        $user = $token->getUser();
        // if the user is anonymous, do not grant access
        if (!$user instanceof UserInterface) {
            return false;
        }

        switch ($attribute) {
            case 'BOARD_CREATE':
                return $user->isVerified();
            case 'BOARD_MANAGE':
                return $user == $subject->getUser();
        }

        return false;
    }
}
