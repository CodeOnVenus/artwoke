<a name="readme-top"></a>


<!-- PROJECT LOGO -->
<br />
<div align="center">

  <a href="https://gitlab.com/MahaBcb.dev/artwoke">
    <img src="public/capture.png" alt="capture">
  </a>
  <p> deploy by heroku </p>
</div>

<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
        <li><a href="#lauch">Lauch the app</a></li>
      </ul>
    </li>

  </ol>
</details>


## About The Project
  <p>Artwoke is a social network (pinterest like) where you can share your art with the world. 
  Create an account, add a board and share it. 
  Inspire the world and be inspired by the art of others</p>

Use the `BLANK_README.md` to get started.

<p align="right">(<a href="#readme-top">back to top</a>)</p>



### Built With

This section should list any major frameworks/libraries used to bootstrap your project. Leave any add-ons/plugins for the acknowledgements section. Here are a few examples.

* Symfony [https://symfony.com]
* Webpack Encore [https://github.com/symfony/webpack-encore]
* Docker [https://www.docker.com]
* PHPUnit [https://phpunit.de]
* Mysql [https://www.mysql.com/fr/]
* AWS-S3 [https://aws.amazon.com/fr/s3/]
* Gitlab-CI [https://docs.gitlab.com/ee/ci/]

<p align="right">(<a href="#readme-top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

### Prerequisites

  ```sh
 php 8.0.23
 Composer
 Mysql
 npm
 docker
 symfony cli
  ```


### Installation

_Below is an example of how you can instruct your audience on installing and setting up your app. This template doesn't rely on any external dependencies or services._

1. Clone the repo
   ```sh
   git clone https://gitlab.com/MahaBcb.dev/artwoke.git
   ```
2. Install Composer packages
   ```sh
   composer install
   ```
3. Install npm packages
   ```sh
   npm install
   ```
### Lauch the app

1. MYSQL with docker
   ```sh
   docker-compose up -d
   ```
2. Lanch the database
  ```sh
  symfony console doctrine:database:create
  symfony console doctrine:migrations:migrate
  ```
3. Server with Symfony CLI
   ```sh
   symfony serve -d
   ```

### That's it

Go to https://localhost:8000 register yourself and enjoy

<p align="right">(<a href="#readme-top">back to top</a>)</p>


### Next release
- improve profile page
- like a board
- search with typesense
- sharing on social media
- login with google or facebook
- notifications with notyf
- mise en cache
- change slug
- Voter on edit account

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#readme-top">back to top</a>)</p>

<!-- CONTACT -->
## Contact

Maha Bouchiba - maha.bouchiba@gmail.com

<p align="right">(<a href="#readme-top">back to top</a>)</p>
