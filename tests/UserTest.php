<?php

namespace App\Tests;

use App\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testIsTrue(): void
    {
        $user = new User();
        $user->setName('John Doe')
            ->setPassword('password')
            ->setEmail('john.doe@mail.com')
            ->setDescription('description')
            ->setImage('image.jpg');

        $this->assertTrue($user->getName() == 'John Doe', "L'assertion est juste");
        $this->assertTrue($user->getPassword() == 'password');
        $this->assertTrue($user->getDescription() == "description");
        $this->assertTrue($user->getImage() == "image.jpg");
    }

    public function testIsFalse(): void
    {
        $user = new User();
        $user->setName('John Doe')
            ->setPassword('password')
            ->setEmail('john.doe@mail.com')
            ->setDescription('description')
            ->setImage('image.jpg');

        $this->assertFalse($user->getName() == 'false');
        $this->assertFalse($user->getPassword() == 'false');
        $this->assertFalse($user->getDescription() == "false");
        $this->assertFalse($user->getImage() == "false");
    }

    public function testIsEmpty(): void
    {
        $user = new User();

        $this->assertEmpty($user->getName());
        $this->assertEmpty($user->getPassword());
        $this->assertEmpty($user->getDescription());
        $this->assertEmpty($user->getImage());
    }
}
