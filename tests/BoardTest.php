<?php

namespace App\Tests;

use DateTime;
use App\Entity\User;
use App\Entity\Boards;
use PHPUnit\Framework\TestCase;

class BoardTest extends TestCase
{
    public function testiSTrue(): void
    {
        $user = new User();
        $user->setName('John Doe')
            ->setPassword('password')
            ->setEmail('john.doe@mail.com')
            ->setDescription('description')
            ->setImage('image.jpg');
        $board = new Boards();
        $board->setTitle('title')
                ->setDescription('description')
                ->setCreatedAt(new DateTime())
                ->setImage('image');
        $this->assertTrue($board->getTitle() == 'title');
        $this->assertTrue($board->getDescription() == 'description');
        $this->assertTrue($board->getImage() == "image");
    }

    public function testiSFalse(): void
    {
        $user = new User();
        $user->setName('John Doe')
            ->setPassword('password')
            ->setEmail('john.doe@mail.com')
            ->setDescription('description')
            ->setImage('image.jpg');

        $board = new Boards();
        $board->setTitle('title')
                ->setDescription('description')
                ->setCreatedAt(new DateTime())
                ->setImage('image')
        ;
        $this->assertFalse($board->getTitle() == 'false');
        $this->assertFalse($board->getDescription() == 'false');
        $this->assertFalse($board->getImage() ==  "false");
    }

    public function testiSEmpty(): void
    {
        $user = new User();
        $board = new Boards();

        $this->assertEmpty($board->getTitle());
        $this->assertEmpty($board->getDescription());
        $this->assertEmpty($board->getImage());
    }
}
